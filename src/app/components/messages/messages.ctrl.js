import close from '../../../assets/icons/close.svg'
import menu from '../../../assets/icons/menu.svg'
import logout from '../../../assets/icons/logout.svg'

class MessagesCtrl {
    constructor($scope, $q, $http, $location, store, usersService, requestFactory, messagesService) {
        $scope.staffer = store.get('staffer');
        $scope.close = close;
        $scope.menu = menu;
        $scope.logoutIcon = logout;
        $scope.url = requestFactory.url;
        $scope.searchKeyWord = "";
        $scope.messages = [];
        $scope.count = 0;

        console.log("11111");
        $scope.menutexp = function() {
            var aside = angular.element( document.querySelector( '#aside' ) );
            var pageover = angular.element( document.querySelector( '#pageover' ) );
            aside.addClass('open');
            pageover.addClass('visible');
        };

        $scope.pageexp = function() {
            var aside = angular.element( document.querySelector( '#aside' ) );
            var pageover = angular.element( document.querySelector('#pageover'));

            pageover.removeClass('visible');
            if(aside.hasClass('open')){
                aside.removeClass('open')
            }
        };

        $scope.logout = function () {
            store.remove('token');
            store.remove('staffer');
            store.remove('role');
        };


        console.log("22222");

        messagesService.readMessages($scope.staffer.id).then(function (result) {
        }, function (result) {
            requestFactory.errorCallback(result);
        });

        $q.all([messagesService.messages($scope.staffer.id),usersService.users()]).then(([messages, users]) => {
            $scope.messages = messages.data.map(message => {
                message.user = users.data.find(({ id }) => message.senderId === id);
                return message
            });
        }, function (result) {
            console.log("not succ"+result);
            requestFactory.errorCallback(result);
        });

        messagesService.messagesCount($scope.staffer.id)
            .then(function (count) {
                $scope.count = count.data
            })
    }
}

export default MessagesCtrl;