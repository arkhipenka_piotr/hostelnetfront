import template from './messages.tmpl.html'
import controller from './messages.ctrl.js'

export default {
    template: template,
    controller: ['$scope', '$q', '$http', '$location', 'store', 'usersService', 'requestFactory', 'messagesService', controller]
}