import close from '../../../assets/icons/close.svg'
import menu from '../../../assets/icons/menu.svg'
import logout from '../../../assets/icons/logout.svg'
import delicon from '../../../assets/icons/delete.svg'
import edit from '../../../assets/icons/edit.svg'

class MessageCtrl {
    constructor($scope, $http, $location,  store, $stateParams, messagesService, requestFactory) {
        $scope.staffer = store.get('staffer');
        $scope.close = close;
        $scope.menu = menu;
        $scope.logoutIcon = logout;
        $scope.delicon = delicon;
        $scope.edit = edit;
        $scope.url = requestFactory.url;
        $scope.receiverId = $stateParams.param;
        $scope.message = "";
        $scope.senderId = $scope.staffer.id;
        $scope.count = 0;

        $scope.menutexp = function() {
            var aside = angular.element( document.querySelector( '#aside' ) );
            var pageover = angular.element( document.querySelector( '#pageover' ) );
            aside.addClass('open');
            pageover.addClass('visible');
        };

        $scope.pageexp = function() {
            var aside = angular.element( document.querySelector( '#aside' ) );
            var pageover = angular.element( document.querySelector('#pageover'));

            store.remove('token');
            store.remove('role');
            store.remove('staffer');

            pageover.removeClass('visible');
            if(aside.hasClass('open')){
                aside.removeClass('open')
            }
        };

        $scope.sendMessage = function () {
            messagesService.sendMessage($scope.message, $scope.senderId, $scope.receiverId)
                .then(function (message) {
                    $location.path('messages');
                })
        };

        messagesService.messagesCount($scope.staffer.id)
            .then(function (count) {
                $scope.count = count.data
            })
    }
}

export default MessageCtrl;