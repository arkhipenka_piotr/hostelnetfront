import template from './message.tmpl.html'
import controller from './message.ctrl.js'

export default {
    template: template,
    controller: ['$scope', '$http', '$location',  'store', '$stateParams', 'messagesService', 'requestFactory',controller]
}