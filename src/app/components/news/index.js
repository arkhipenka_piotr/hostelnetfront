import template from './news.tmpl.html'
import controller from './news.ctrl.js'

export default {
    template: template,
    controller: ['$scope', '$http', '$location',  'store', '$stateParams', 'newsService', 'requestFactory', 'messagesService', '$sce', controller]
}