import template from './users.tmpl.html'
import controller from './users.ctrl.js'

export default {
    template: template,
    controller: ['$scope', '$q', '$http', '$location', 'store', 'usersService', 'requestFactory', 'messagesService', 'addressService',controller]
}