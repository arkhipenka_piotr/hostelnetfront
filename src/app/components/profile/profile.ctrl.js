import close from '../../../assets/icons/close.svg'
import menu from '../../../assets/icons/menu.svg'
import logout from '../../../assets/icons/logout.svg'

class ProfileCtrl {
    constructor($scope, $http, $location, store, $stateParams, usersService, requestFactory, messagesService, addressService) {
        $scope.staffer = store.get('staffer');
        $scope.role = store.get('role');
        $scope.close = close;
        $scope.menu = menu;
        $scope.logoutIcon = logout;
        $scope.url = requestFactory.url;
        $scope.birthDate = new Date();

        $scope.count = 0;
        $scope.imgfile = {};

        $scope.menutexp = function() {
            var aside = angular.element( document.querySelector( '#aside' ) );
            var pageover = angular.element( document.querySelector( '#pageover' ) );
            aside.addClass('open');
            pageover.addClass('visible');
        };

        $scope.pageexp = function() {
            var aside = angular.element( document.querySelector( '#aside' ) );
            var pageover = angular.element( document.querySelector('#pageover'));

            pageover.removeClass('visible');
            if(aside.hasClass('open')){
                aside.removeClass('open')
            }
        };

        $scope.logout = function () {
            store.remove('token');
            store.remove('staffer');
            store.remove('role');
        };


        console.log("Param: "+$stateParams.param);
        if ($stateParams.param==0){
            $scope.profile = $scope.staffer;
            addressService.address($scope.profile.address_id).then(function (result) {
                $scope.address = result.data;
            });
        }
        else{
            usersService.user($stateParams.param).then(function (result) {
                $scope.profile = result.data;
                $scope.birthDate = new Date(result.data.dateOfBirth);

                addressService.address($scope.profile.address_id).then(function (result) {
                    $scope.address = result.data;
                    console.log("LALALALA" + result.data.dateOfBirth);
                    console.log($scope.birthDate)
                });
            }, function (result) {
                requestFactory.errorCallback(result);
            })
        }

        $scope.isAdmin = function () {
            return $scope.role == 'admin'||$scope.profile===$scope.staffer;
        };

        $scope.postChanges = function () {
            addressService.postAddress($scope.address);
            usersService.postUser($scope.profile);
        };
        
        $scope.saveImg = function () {
            if ($scope.imgfile.size<5242880) {
                usersService.postImage($scope.imgfile, $scope.encoded_file).then(function (result) {
                    $scope.profile.photoUrl = "http://localhost:18783/images/" + result.data.url;
                });
            }
        };

        $scope.onFileSelect = function($file){
            $scope.imgfile = $file;
            var reader = new FileReader();
            reader.onload = function(e){
                console.log("about to encode");
                $scope.encoded_file = btoa(e.target.result.toString());
            };
            reader.readAsBinaryString($file);
        };

        messagesService.messagesCount($scope.staffer.id)
            .then(function (count) {
                $scope.count = count.data
            })
    }
};

export default ProfileCtrl;