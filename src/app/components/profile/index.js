import template from './profile.tmpl.html'
import controller from './profile.ctrl.js'

export default {
    template: template,
    controller: ['$scope', '$http', '$location', 'store', '$stateParams', 'usersService', 'requestFactory', 'messagesService', 'addressService', controller]
}