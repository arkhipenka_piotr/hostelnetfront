import template from './login.tmpl.html'
import controller from './login.ctrl.js'

export default {
    template: template,
    controller: ['$scope', '$http', 'loginService', '$location', 'store', 'usersService',controller]
}