import template from './edit.tmpl.html'
import controller from './edit.ctrl.js'

export default {
    template: template,
    controller: ['$scope', '$http', '$location',  'store', '$stateParams', 'newsService', 'requestFactory', 'messagesService',controller]
}