var messagesFactory = function ($http, $httpParamSerializer, requestFactory) {
    const API_BASE = "http://localhost:18790/";
    return {
        messages : function(id) {
            var request = {
                method: 'GET',
                url: requestFactory.url(API_BASE+'messages/'+id)
            };
            return $http(request);
        },
        sendMessage: function (message, senderId, receiverId) {
            var request = {
                method: 'POST',
                url: requestFactory.url(API_BASE+'messages'),
                // headers: {
                //     "Content-type": "application/x-www-form-urlencoded; charset=utf-8"
                // },
                data:{
                    message: message,
                    senderId: senderId,
                    receiverId: receiverId
                }
            };
            return $http(request);
        },
        messagesCount : function(id) {
            var request = {
                method: 'GET',
                url: requestFactory.url(API_BASE+'messages/'+id+'/count')
            };
            return $http(request);
        },
        readMessages : function(id) {
            var request = {
                method: 'POST',
                url: requestFactory.url(API_BASE+'messages/'+id+'/read')
            };
            return $http(request);
        },
    };
};
messagesFactory.$inject = ['$http', '$httpParamSerializer','requestFactory'];

export default messagesFactory